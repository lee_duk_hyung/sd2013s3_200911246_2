import java.util.Date;
import java.awt.Point;
import java.awt.Color;

public class SD2013s3_200911246_Tester{
	public static void main(String[] args){
		//ch2_1main();
		//ch2_3main();
		//ch2_8main();
		ch4_2main();
	}

	/*private static void ch2_10 main(){
		Driver d=new Driver();
		Automobile[] fleet=new Automobile[3];

		fleet[0]=new Sedan(5);
		fleet[1]=new Minivan(7);
		fleet[2]=new SportsCar(2);

		int totalCapacity=0;
		for(int i=0;i<fleet.length;i++){
			if(fleet[i] instanceof Sedan)
				totalCapacity+=fleet[i].getCapacity();
			else if(fleet[i] instanceof Minivan)
				totalCapacity+=fleet[i].getCapacity();
			else if(fleet[i] instanceof SportsCar)
				totalCapacity+=fleet[i].getCapacity();
		}*/

	
	 private static void ch4_2main() {
        //---(1) Ch4Triangle version 1 equals() 테스트
       /* System.out.println("===Ch4Triangle equals() version 1 #1===");
        Ch4Triangle t1 = new Ch4Triangle(new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        Ch4ColoredTriangle rct1 = new Ch4ColoredTriangle(Color.red, 
                    new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        System.out.println("t1.equals(rct1): "+t1.equals(rct1));
        System.out.println("rct1.equals(t1): "+rct1.equals(t1));*/

       /* /---(1-1) (1) 문제점 확인
        //---색이 틀린데, true가 찍혀. symmetric, transitive, consistent 만족하지만... 
        System.out.println("===Ch4Triangle equals() version 1 #2===");
        Ch4ColoredTriangle rct1_1 = new Ch4ColoredTriangle(Color.red, 
                    new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        Ch4ColoredTriangle bct1_1 = new Ch4ColoredTriangle(Color.blue, 
                    new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        System.out.println("rct1_1.equals(bct1_1): "+rct1_1.equals(bct1_1));
        System.out.println("bct1_1.equals(rct1_1): "+bct1_1.equals(rct1_1));*/
        //
      /*  //---(2) 위 문제점 해결 Ch4ColoredTriangle version 1 equals() 테스트
        System.out.println("===Ch4Triangle + Ch4ColoredTriangle equals() version 1===");
        Ch4Triangle t2 = new Ch4Triangle(new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        Ch4ColoredTriangle rct2 = new Ch4ColoredTriangle(Color.red, 
                    new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        System.out.println("t2.equals(rct2): "+t2.equals(rct2));
        System.out.println("rct2.equals(t2): "+rct2.equals(t2));*/
        
        //---(3) Ch4ColoredTriangle version 2 equals() 테스트
        //---(4) test
        System.out.println("===Ch4Triangle + Ch4ColoredTriangle equals() version 2===");
        Ch4Triangle t3 = new Ch4Triangle(new Point(0,0), new Point(1,1),
                     new Point(2,2));
        Ch4ColoredTriangle rct3 = new Ch4ColoredTriangle(Color.red, 
                    new Point(0,0), new Point(1,1),
                                                            new Point(2,2));
        Ch4ColoredTriangle bct3 = new Ch4ColoredTriangle(Color.blue, 
                                                            new Point(0,0), new Point(1,1),
                                                            new Point(2,2));
        System.out.println("rct3.equals(t3): "+rct3.equals(t3));
        System.out.println("t3.equals(bct3): "+t3.equals(bct3));
        System.out.println("rct3.equals(bct3): "+rct3.equals(bct3));
        
    }

    



	/*private static void ch4_1main(){
		String a = "SD2013";
		String b = "SD2013";
		System.out.println("'String a' hashcode: "+System.identityHashCode(a));
		System.out.println("'String b' hashcode: "+System.identityHashCode(b));
		System.out.println("'a==b: '"+(a==b));
		System.out.println("'a.equals(b): '" +a.equals(b));
		String c = new String("SD2013");
		String d = new String("SD2013");
		System.out.println("'String c' hashcode: "+System.identityHashCode(c));
		System.out.println("'String d' hashcode: "+System.identityHashCode(d));
		System.out.println("'c==d: '"+(c==d));
		System.out.println("'c.equals(d): '" +c.equals(d));
		
		int e = 1;
		int f = 1;
		System.out.println("'int e' hashcode: "+System.identityHashCode(e));
		System.out.println("'int f' hashcode: "+System.identityHashCode(f));
		System.out.println("'e==f: '"+(e==f));
		//System.out.println("'a.equals(b): '" +a.equals(b));
		Integer g = new Integer (1);
		Integer h = new Integer (1);
		System.out.println("'integer g' hashcode: "+System.identityHashCode(g));
		System.out.println("'integer h' hashcode: "+System.identityHashCode(h));
		System.out.println("'g==h: '"+(g==h));
		//System.out.println("'c.equals(d): '" +c.equals(d));
	}
	
	private static void ch2_9main(){
	System.out.println("-- ch2_9 Override --");

	Object3 o=new Object3();
	Automobile3 auto=new Automobile3();
	Object3 autoObject=new Automobile3();

	auto.equals3(o); //(3)
	auto.equals3(auto); //(2)
	auto.equals3(autoObject); //(3)
	o.equals(o); //(1)
	o.equals(auto); //(1)
	o.equals(autoObject); //(1)
	autoObject.equals3(o); //(3)
	autoObject.equals3(auto); //(3)
	autoObject.equals3(autoObject); //(3)
	}
	
	/*private static void ch2_8main(){
	System.out.println ("-- ch 2_8 Overload --");

	Object2 o=new Object2();
	Automobile2 auto=new Automobile2();
	Object2 autoObject=new Automobile2();

	auto.equals2(o);
	auto.equals2(auto);
	auto.equals2(autoObject);
	System.out.println("===========================");
	o.equals2(o);
	o.equals2(auto);
	o.equals2(autoObject);
	autoObject.equals2(o);
	autoObject.equals2(o);
	autoObject.equals2(autoObject);
	}*/
 
	/*private static void ch2_3main(){
		Driver d=new Driver();
		d.moveCar(new Sedan());
		d.moveCar(new Minivan());
		d.moveCar(new SportsCar());
	}
	private static void ch2main(){
		System.out.println("----ch2 Person----");
		Person fp=new Person("Lee duk hyung",new Date(0));
		String rName=fp.getName();
		System.out.println(rName);
	}
	
	private static void ch3main(){
		System.out.println("----ch2 interface----");
		SimpleRuner sr = new SimpleRuner();
		sr.run();

		Runnable rn = new SimpleRuner();
		rn.run();
	}
	private static void ch2_2main(){
		System.out.println("interface");
		Colorable pt = new PlainText();
		Colorable ct = new CompoundText();
		Colorable cc = new Circle();

		Changeable cc2 =  new Circle();
		//Colorable cr4 = new Changeable ();
	}
	private static void ch2_1main(){
		Account ac=new Account();
		Account ca=new ChekingAccount();
		Account cla=new CreditLineAccount();

		Account ctca=new ChekingTrafficCardAccount();
	}
}*/
}
