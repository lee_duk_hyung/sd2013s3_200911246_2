package com.example.dietdiary;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class Nextmain extends TabActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nextmain);
		 TabHost mTabHost = getTabHost();
	        
	        mTabHost.addTab(mTabHost.newTabSpec("tab_test1")
	        		.setIndicator("일기,kcal 입력")
	        		.setContent(R.id.view1)
	        		);
	        
	        TabSpec tabSpec = mTabHost.newTabSpec("tab_test2");
	        tabSpec.setIndicator("Map");
	        Context ctx = this.getApplicationContext();
	        Intent i = new Intent(ctx, Maptab.class);
	        tabSpec.setContent(i);
	        mTabHost.addTab(tabSpec);
	    
	       
	        
//	        mTabHost.addTab(mTabHost.newTabSpec("tab_test2")
//	        		.setIndicator("지도")
//	        		.setContent(R.id.view2)
//	        		);
//	        
	        
	        mTabHost.addTab(mTabHost.newTabSpec("tab_test3")
	        		.setIndicator("오늘흡입량")
	        		.setContent(R.id.view3)
	        		);
	        mTabHost.addTab(mTabHost.newTabSpec("tab_test4")
	        		.setIndicator("칼로리리스트")
	        		.setContent(R.id.view4)
	        		);
	                
	        //mTabHost.setCurrentTab(0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.nextmain, menu);
		return true;
	}

}
