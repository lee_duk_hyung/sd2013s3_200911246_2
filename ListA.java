package list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

public class ListA{
	public static void main(String[] args)
	{
		List<Integer>list=new LinkedList<Integer>();

		list.add(new Integer(1));
		list.add(new Integer(2));
		list.add(new Integer(3));

		Iterator<Integer> itr=list.iterator();
		Iterator<Integer> itr1=list.iterator();

		System.out.println("list size: "+list.size());
		System.out.println();

		while(itr.hasNext())
		{
			Integer curInt=itr.next();
			System.out.println(curInt.intValue()+" ");
		}
		System.out.println("CLEAR SUCCESS");
		System.out.println();
		
		while(itr1.hasNext())
		{
			Integer curInt=itr1.next();
			if(curInt.intValue()==2)
			{
				System.out.println(curInt.intValue()+" ");
				
				System.out.println("SUCCESS");
			}
		}
		list.clear();
		if(list.isEmpty()){
			System.out.println("CLEAR SUCCESS");
		}
	}
}
