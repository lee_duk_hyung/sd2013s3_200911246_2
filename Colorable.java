interface Colorable {}

class PlainText implements Colorable {}

interface Changeable extends Colorable {}

class CompoundText extends PlainText {}

class Circle implements Changeable {}
