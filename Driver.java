abstract class Automobile{
	abstract void move();
	abstract void getCapacity();
}

class Sedan extends Automobile{
	
	int capacity;
	

	public void getCapacity(){
		return capacity;
	}

	public void move(){
		System.out.println("Sedan go");
	}
}

class Minivan extends Automobile{
	
	int capacity;

	public void getCapacity(){
		return capacity;
	}
	public void move(){
		System.out.println("Minivan gogo");
	}
}

class SportsCar extends Automobile{

	int capacity;

	public void getCapacity(){
		return capacity;
	}
	public void move(){
		System.out.println("SportsCar gogogo");
	}
}

/*class Driver{
	public void moveCar(Sedan sedan){
		sedan.move();
	}
	public void moveCar(Minivan minivan){
		minivan.move();
	}
	public void moveCar(SportsCar sportscar){
		sportscar.move();
	}
}*/
class Driver{
	public void moveCar(Automobile am){
		am.move();
	}
}:
