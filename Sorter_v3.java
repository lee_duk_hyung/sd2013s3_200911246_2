class Sorter_v3 { // 보라색 new, 붉은색 변경
    public static void main(String[] args) {
        Integer[] numbers = {new Integer(19), new Integer(79), 
                         new Integer(2), new Integer(8)};
        String[] strings = {"Jin", "Adam", "Digital", "Sangmyung"};

        Comparator comp = new Comparator();
                        //--------------------------------------------------------------------------------------------------
        System.out.println("before=====================");
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("numbers["+i+"]="+numbers[i]);
        }
        for (int i = 0; i < strings.length; i++) {
            System.out.println("strings["+i+"]="+strings[i]);
        }
                        //--------------------------------------------------------------------------------------------------
                        ch3Sorter.sort(numbers, comp);
        ch3Sorter.sort(strings, comp);
                        //--------------------------------------------------------------------------------------------------
        System.out.println("after======================");
        
        for (int i = 0; i < numbers.length; i++) {
            System.out.println("numbers["+i+"]="+numbers[i]);
        }
        for (int i = 0; i < strings.length; i++) {
            System.out.println("strings["+i+"]="+strings[i]);
        }
    }
}

class ch3Sorter {
    public static void sort(Object[] data, Comparator comp) {
        for (int i = data.length-1; i >= 1; i--) {
            int indexOfMax = 0;
            for (int j = 1; j <= i; j++) {
                if (comp.compare(data[j], data[indexOfMax]) > 0)
                    indexOfMax = j;
            }
            Object temp = data[i];
            data[i] = data[indexOfMax];
            data[indexOfMax] = temp;
        }
    }
}

 class Comparator {
    public int compare(Object o1, Object o2)
	{
		if(o1 instanceof String && o2 instanceof String)
			return ((String) o1).compareTo((String) o2);
		else if (o1 instanceof Integer && o2 instanceof Integer)
			return (Integer) o1- (Integer) o2;
		else
			return 1;
	}
}

