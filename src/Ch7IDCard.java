public class Ch7IDCard extends Ch7Product {
    private String owner;
    public Ch7IDCard(String owner) {
        System.out.println(owner + "의 카드를 생성.");
        this.owner = owner;
    }
    public void use() {
        System.out.println(owner + "의 카드를 사용.");
    }
    public String getOwner() {
        return owner;
    }
}

