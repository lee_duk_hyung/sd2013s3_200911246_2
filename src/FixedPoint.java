import java.awt.Point;

public final class FixedPoint
{
	private Point pt;
	
	public FixedPoint(Point p)
	{this.pt=new Point(p);}
	public FixedPoint(int x, int y)
	{this.pt=new Point(x,y);}
	public double getX(){return pt.getX();}
	public double getY(){return pt.getY();}
	public Point getLocation(){return pt.getLocation();}
}
