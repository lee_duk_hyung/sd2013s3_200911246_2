public class Ch7PrintBannerInheritance implements Ch7Print{
	Ch7Banner banner;
   public Ch7PrintBannerInheritance(String string) {
        banner=new Ch7Banner (string);
    }
    public void printStrong() {	
       banner. showWithAster();
    }
    public void printWeak() {
       banner. showWithParen();
    }
}

