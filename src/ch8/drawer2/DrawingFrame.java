import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JToolBar;


public class DrawingFrame extends JFrame {

    public DrawingFrame() {

        super("Drawing Application");

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        JComponent drawingCanvas=createDrawingCanvas();

        add(drawingCanvas, BorderLayout.CENTER);

        JToolBar toolbar=createToolbar(drawingCanvas);

        add(toolbar, BorderLayout.NORTH);

    }

    private JComponent createDrawingCanvas() {

        JComponent drawingCanvas=new JToolBar();

        drawingCanvas.setPreferredSize(new Dimension(400, 300));

        drawingCanvas.setBackground(Color.white);

        drawingCanvas.setBorder(BorderFactory.createEtchedBorder());

        return drawingCanvas;

    }

    private JToolBar createToolbar(JComponent canvas) {

        JToolBar toolbar=new JToolBar();

        JButton ellipseButton=new JButton("Ellipse");

        toolbar.add(ellipseButton);

        JButton squareButton=new JButton("Square");

        toolbar.add(squareButton);

        JButton rectButton=new JButton("Rect");

        toolbar.add(rectButton);

      //add the CanvasEditor listener to the canvas and to the buttons,

      //with the ellipseButton initially selected

      final CanvasEditor canvasEditor = new CanvasEditor(new Ellipse(0,0,60,40));

        ellipseButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae){
                canvasEditor.setCurrentFigure(new Ellipse(0, 0, 60, 40));
            }
});

        squareButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                canvasEditor.setCurrentFigure(new Square(0, 10, 60 ));
            }
});

        rectButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                canvasEditor.setCurrentFigure(new Rect(0, 20, 60, 40));
            }
});
        canvas.addMouseListener(canvasEditor);

        return toolbar;

    }

public static void main(String[] args){

        DrawingFrame drawFrame=new DrawingFrame();

        drawFrame.pack();

        drawFrame.setVisible(true);

    }

}
