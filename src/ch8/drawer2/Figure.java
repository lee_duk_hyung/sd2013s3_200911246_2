import java.awt.*;

public abstract class Figure
{
private int centerX, centerY; //the center coordinates
private int width;
private int height;
public Figure(int centerX, int centerY, int w, int h)
{
this.centerX = centerX; this.centerY = centerY;
this.width = w; this.height = h;
}
public int getWidth() { return width; }
public int getHeight() { return height; }
public void setCenter(int centerX, int centerY)
{
this.centerX = centerX; this.centerY = centerY;
}
public int getCenterX() { return centerX; }
public int getCenterY() { return centerY; }
public abstract void draw(Graphics g);
}


class Ellipse extends Figure
{
public Ellipse(int centerX, int centerY, int w, int h)
{
super(centerX, centerY, w, h);
}
public void draw(Graphics g)
{
int width = getWidth();
int height = getHeight();
g.drawOval(getCenterX() - width/2, getCenterY() - height/2, width, height);
}
}
class Rect extends Figure
{
public Rect(int centerX, int centerY, int w, int h)
{
super(centerX, centerY, w, h);
}
public void draw(Graphics g)
{
int width = getWidth();
int height = getHeight();
g.drawRect(getCenterX() - width/2, getCenterY() - height/2, width, height);
}
}
class Square extends Rect
{
public Square(int centerX, int centerY, int w)
{
super(centerX, centerY, w, w);
}
}
