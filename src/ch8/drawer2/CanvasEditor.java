import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CanvasEditor implements MouseListener {
    //instance variables
    private Figure currentFigure;

    //constructor
    public CanvasEditor(Figure figure)     {
        this.currentFigure = figure;
    }

    public void setCurrentFigure(Figure newFigure)     {
        currentFigure = newFigure;
    }

    public void mouseClicked(MouseEvent e)     {
        JPanel canvas = (JPanel) e.getSource();
        currentFigure.setCenter(e.getX(), e.getY());
        currentFigure.draw(canvas.getGraphics());
    }

    @Override
    public void mousePressed(MouseEvent e) { }
    @Override
    public void mouseReleased(MouseEvent e) { }
    @Override
    public void mouseEntered(MouseEvent e) { }
    @Override
    public void mouseExited(MouseEvent e) { }
}

