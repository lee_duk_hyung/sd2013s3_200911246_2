import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class CanvasEditor implements ActionListener, MouseListener {
    private JButton currentButton; //the currently selected button
    public CanvasEditor(JButton initialButton) {
        this.currentButton = initialButton;
    }
  @Override
    public void actionPerformed(ActionEvent ae) {
        currentButton = (JButton) ae.getSource();
    }
 @Override
     public void mouseClicked(MouseEvent e) {
	int x=e.getX();
	int y=e.getY();
	JPanel canvas=(JPanel)e.getSource();
	if (currentButton.getText().equals("Ellipse"))
		canvas.getGraphics().drawOval(x-30,y-20,60,40);
	else if (currentButton.getText().equals("Rect"))
		canvas.getGraphics().drawRect(x-30,y-20,60,40);
	else if (currentButton.getText().equals("Square"))
		canvas.getGraphics().drawRect(x-25,y-25,50,50);
	 }

    @Override
    public void mousePressed(MouseEvent e) { }
    @Override
    public void mouseReleased(MouseEvent e) { }
    @Override
    public void mouseEntered(MouseEvent e) { }
 @Override
    public void mouseExited(MouseEvent e) { }

}

