public class NoQuarterState implements State {
    GumballMachine gumballMachine;
 
    public NoQuarterState(GumballMachine gumballMachine) {
	   this.gumballMachine = gumballMachine;
    }
     // 동전 투입
    public void insertQuarter() {
        System.out.println("동전을 넣으셨습니다.");
        gumballMachine.setState(gumballMachine.getHasQuarterState());
    }
     // 동전 반환
    public void ejectQuarter() {
        System.out.println("동전을 넣어주세요.");
    }
     // 손잡이 돌리기
    public void turnCrank() {
        System.out.println("동전을 넣어주세요.");
     }
     // 알맹이 꺼내기
    public void dispense() {
        System.out.println("동전을 넣어주세요.");
    } 
 
    public String toString() {
        return "waiting for quarter";
    }
}

