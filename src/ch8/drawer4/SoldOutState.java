public class SoldOutState implements State{
	GumballMachine gumballMachine;
	public SoldOutState(GumballMachine gumballMachine){
		this.gumballMachine=gumballMachine;
	}

	public void insertQuarter()
	{
		System.out.println("매진 되었습니다.");
	}
		
	public void ejectQuarter()
	{
		System.out.println("남은 알맹이가 없습니다.");
	}
	public void turnCrank()
	{
		System.out.println("상품이 매진되었습니다.");
	}
	public void dispense(){}
}


	
