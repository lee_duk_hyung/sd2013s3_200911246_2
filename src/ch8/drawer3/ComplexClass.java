public class ComplexClass implements Cloneable {
    private String universityName;
    private String department;
    private String grade;
    
    private String name;
    
    public ComplexClass(String uniName, String depart, String grade) {
        this.universityName = uniName;
        this.department = depart;
        this.grade = grade;
    }

    public void setName(String name){
        this.name = name;
    }
    
    public String getName() {
        return name+" "+universityName;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        ComplexClass cc = (ComplexClass)super.clone();
        return cc;
    }
}

