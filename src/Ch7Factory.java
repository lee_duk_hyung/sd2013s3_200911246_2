public abstract class Ch7Factory{
	public final Ch7Product create(String owner){
		Ch7Product p=createProduct(owner);
		registerProduct(p);
       		return p;
    }
    protected abstract Ch7Product createProduct(String owner);
    protected abstract void registerProduct(Ch7Product product);
}

