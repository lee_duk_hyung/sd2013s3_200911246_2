import java.util.Date;
import java.awt.Point;
import java.awt.Color;

public class SD2013s3_200911246_Tester{
	public static void main(String[] args){
		//ch2_1main();
		//ch2_3main();
		//ch2_8main();
		//ch4_2main();
		//ch5_1main();
		//ch5_2main();
		//ch6_1main();
		//ch7_1main();
		//ch7_2main();
		//ch7_3main();
		//Ch7_4main();
		Ch7_5main();
	}


	private static void Ch7_5main(){
		Ch7Factory factory = new Ch7IDCardFactory();
       		Ch7Product card1 = factory.create("나소설");
        	Ch7Product card2 = factory.create("너소설");
        	Ch7Product card3 = factory.create("그소설");

        	card1.use();
        	card2.use();
        	card3.use();
	}
}

	
	/*private static void Ch7_4main(){
	Ch7SimpleRemoteControl remote=new Ch7SimpleRemoteControl();
	Ch7Light light = new Ch7Light();
        
        Ch7LightOnCommand lightOn = new Ch7LightOnCommand(light);

        remote.setCommand(lightOn);
        remote.buttonWasPressed();

	Ch7LightOffCommand lightOff = new Ch7LightOffCommand(light);      

        remote.setCommand(lightOff);
        remote.buttonWasPressed();

        Ch7GarageDoor gdoor = new Ch7GarageDoor(); 
        Ch7GarageDoorOpenCommand garageOpen = new Ch7GarageDoorOpenCommand(gdoor);
        remote.setCommand(garageOpen);
        remote.buttonWasPressed();

	Ch7GarageDoor hdoor = new Ch7GarageDoor();
	Ch7GarageDoorDownCommand garageDown = new Ch7GarageDoorDownCommand(hdoor);
	remote.setCommand(garageDown);
	remote.buttonWasPressed();
}
	
	/*private static void ch7_3main(){
		Ch7BookShelfCollection bookShelfCollection = new Ch7BookShelfCollection(3);
        bookShelfCollection.appendBook(new Ch7Book("소프트웨어공학"));
        bookShelfCollection.appendBook(new Ch7Book("소프트웨어설계"));
        bookShelfCollection.appendBook(new Ch7Book("프로그래밍"));

        Ch7Iterator it = bookShelfCollection.iterator();
        while (it.hasNext()) {
            Ch7Book book = (Ch7Book) it.next();
            System.out.println(book.getName());
        }
}

	/*private static void ch7_2main(){
		System.out.println("ch7 -- NO Singleton --");
		Ch7SingPatternLoggerBefore loggerB=new Ch7SingPatternLoggerBefore();
		loggerB.readEntireLog();
		System.out.println("ch7 -- Singleton --");
		Ch7SingPatternLoggerBefore.getInstance().readEntireLog();
	}

	/*private static void ch7_1main(){
		System.out.println("Singleton Pattern");
		Ch7Singleton obj1=Ch7Singleton.instance();
		Ch7Singleton obj2=Ch7Singleton.instance();
	}*/

/*private static void ch6_1main(){
Ch7Print p = new Ch7PrintBannerInheritance("Hello");
        p.printStrong();
        p.printWeak();
}
}
	
	/*private static void ch5_2main(){
		System.out.println("==ch5 Immutable");
		System.out.println("1) new FixedPoint w/o inheritance no way to change vars");
		Ch5FixedPointv1New fpv1=new Ch5FixedPointv1New(3,4);
		System.out.printf("Expected 4==%2.1f\n",fpv1.getY());
		System.out.printf("Expected 3==%2.1f\n ",fpv1.getX());

		System.out.println("2)inheritance not immutable");
		Ch5FixedPointv2Inheritance fpv2=new Ch5FixedPointv2Inheritance(3,4);
		fpv2.setLocation(5,6);
		System.out.printf("Expected 5==%2.1f\n",fpv2.getX());
		fpv2.x=7;
		System.out.printf("Expected 7==%2.1f\n",fpv2.getX());

		System.out.println("3)Association mutable");
		Point p = new Point(3,4);
		FixedPoint fp = new FixedPoint(p);
		System.out.println(fp.getX());
		p.x= 5;
		System.out.println(fp.getX());
	}
}*/
	/*private static void ch5_1main(){

		// String class is ‘Immutable’
		String s = "sm";
		System.out.println("s = "+s);
		System.out.println("s hashcode = "+System.identityHashCode(s));
		s=s.concat("univ");
		System.out.println("s (concat)= "+s);
		System.out.println("s (concat)hashcode= "+System.identityHashCode(s));
		s=s.replace('u','x');
		System.out.println("s (replace)= "+s);
		System.out.println("s (replace)hashcode= "+System.identityHashCode(s));
		// concat(추가문자열:String), replace(변경될문자열:String, 변경문자열:String) in String class

		// StringBuffer class is ‘mutable’
		StringBuffer sb = new StringBuffer("sm");
		System.out.println("sb = "+sb);
		System.out.println("sb hashcode = "+System.identityHashCode(sb));
		sb=sb.append("univ");
		System.out.println("sb (append)= "+sb);
		System.out.println("sb (append) hashcode= "+System.identityHashCode(sb));
		// append(추가문자열:String) in StringBuffer class
}
*/
	/*private static void ch2_10 main(){
		Driver d=new Driver();
		Automobile[] fleet=new Automobile[3];

		fleet[0]=new Sedan(5);
		fleet[1]=new Minivan(7);
		fleet[2]=new SportsCar(2);

		int totalCapacity=0;
		for(int i=0;i<fleet.length;i++){
			if(fleet[i] instanceof Sedan)
				totalCapacity+=fleet[i].getCapacity();
			else if(fleet[i] instanceof Minivan)
				totalCapacity+=fleet[i].getCapacity();
			else if(fleet[i] instanceof SportsCar)
				totalCapacity+=fleet[i].getCapacity();
		}*/

	
	/* private static void ch4_2main() {
        //---(1) Ch4Triangle version 1 equals() 테스트
       /* System.out.println("===Ch4Triangle equals() version 1 #1===");
        Ch4Triangle t1 = new Ch4Triangle(new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        Ch4ColoredTriangle rct1 = new Ch4ColoredTriangle(Color.red, 
                    new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        System.out.println("t1.equals(rct1): "+t1.equals(rct1));
        System.out.println("rct1.equals(t1): "+rct1.equals(t1));*/

       /* /---(1-1) (1) 문제점 확인
        //---색이 틀린데, true가 찍혀. symmetric, transitive, consistent 만족하지만... 
        System.out.println("===Ch4Triangle equals() version 1 #2===");
        Ch4ColoredTriangle rct1_1 = new Ch4ColoredTriangle(Color.red, 
                    new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        Ch4ColoredTriangle bct1_1 = new Ch4ColoredTriangle(Color.blue, 
                    new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        System.out.println("rct1_1.equals(bct1_1): "+rct1_1.equals(bct1_1));
        System.out.println("bct1_1.equals(rct1_1): "+bct1_1.equals(rct1_1));*/
        //
      /*  //---(2) 위 문제점 해결 Ch4ColoredTriangle version 1 equals() 테스트
        System.out.println("===Ch4Triangle + Ch4ColoredTriangle equals() version 1===");
        Ch4Triangle t2 = new Ch4Triangle(new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        Ch4ColoredTriangle rct2 = new Ch4ColoredTriangle(Color.red, 
                    new Point(0,0), new Point(1,1), 
                    new Point(2,2));
        System.out.println("t2.equals(rct2): "+t2.equals(rct2));
        System.out.println("rct2.equals(t2): "+rct2.equals(t2));*/
        
        //---(3) Ch4ColoredTriangle version 2 equals() 테스트
        //---(4) test
     /*   System.out.println("===Ch4Triangle + Ch4ColoredTriangle equals() version 2===");
        Ch4Triangle t3 = new Ch4Triangle(new Point(0,0), new Point(1,1),
                     new Point(2,2));
        Ch4ColoredTriangle rct3 = new Ch4ColoredTriangle(Color.red, 
                    new Point(0,0), new Point(1,1),
                                                            new Point(2,2));
        Ch4ColoredTriangle bct3 = new Ch4ColoredTriangle(Color.blue, 
                                                            new Point(0,0), new Point(1,1),
                                                            new Point(2,2));
        System.out.println("rct3.equals(t3): "+rct3.equals(t3));
        System.out.println("t3.equals(bct3): "+t3.equals(bct3));
        System.out.println("rct3.equals(bct3): "+rct3.equals(bct3));
        
    }*/

    



	/*private static void ch4_1main(){
		String a = "SD2013";
		String b = "SD2013";
		System.out.println("'String a' hashcode: "+System.identityHashCode(a));
		System.out.println("'String b' hashcode: "+System.identityHashCode(b));
		System.out.println("'a==b: '"+(a==b));
		System.out.println("'a.equals(b): '" +a.equals(b));
		String c = new String("SD2013");
		String d = new String("SD2013");
		System.out.println("'String c' hashcode: "+System.identityHashCode(c));
		System.out.println("'String d' hashcode: "+System.identityHashCode(d));
		System.out.println("'c==d: '"+(c==d));
		System.out.println("'c.equals(d): '" +c.equals(d));
		
		int e = 1;
		int f = 1;
		System.out.println("'int e' hashcode: "+System.identityHashCode(e));
		System.out.println("'int f' hashcode: "+System.identityHashCode(f));
		System.out.println("'e==f: '"+(e==f));
		//System.out.println("'a.equals(b): '" +a.equals(b));
		Integer g = new Integer (1);
		Integer h = new Integer (1);
		System.out.println("'integer g' hashcode: "+System.identityHashCode(g));
		System.out.println("'integer h' hashcode: "+System.identityHashCode(h));
		System.out.println("'g==h: '"+(g==h));
		//System.out.println("'c.equals(d): '" +c.equals(d));
	}
	
	private static void ch2_9main(){
	System.out.println("-- ch2_9 Override --");

	Object3 o=new Object3();
	Automobile3 auto=new Automobile3();
	Object3 autoObject=new Automobile3();

	auto.equals3(o); //(3)
	auto.equals3(auto); //(2)
	auto.equals3(autoObject); //(3)
	o.equals(o); //(1)
	o.equals(auto); //(1)
	o.equals(autoObject); //(1)
	autoObject.equals3(o); //(3)
	autoObject.equals3(auto); //(3)
	autoObject.equals3(autoObject); //(3)
	}
	
	/*private static void ch2_8main(){
	System.out.println ("-- ch 2_8 Overload --");

	Object2 o=new Object2();
	Automobile2 auto=new Automobile2();
	Object2 autoObject=new Automobile2();

	auto.equals2(o);
	auto.equals2(auto);
	auto.equals2(autoObject);
	System.out.println("===========================");
	o.equals2(o);
	o.equals2(auto);
	o.equals2(autoObject);
	autoObject.equals2(o);
	autoObject.equals2(o);
	autoObject.equals2(autoObject);
	}*/
 
	/*private static void ch2_3main(){
		Driver d=new Driver();
		d.moveCar(new Sedan());
		d.moveCar(new Minivan());
		d.moveCar(new SportsCar());
	}
	private static void ch2main(){
		System.out.println("----ch2 Person----");
		Person fp=new Person("Lee duk hyung",new Date(0));
		String rName=fp.getName();
		System.out.println(rName);
	}
	
	private static void ch3main(){
		System.out.println("----ch2 interface----");
		SimpleRuner sr = new SimpleRuner();
		sr.run();

		Runnable rn = new SimpleRuner();
		rn.run();
	}
	private static void ch2_2main(){
		System.out.println("interface");
		Colorable pt = new PlainText();
		Colorable ct = new CompoundText();
		Colorable cc = new Circle();

		Changeable cc2 =  new Circle();
		//Colorable cr4 = new Changeable ();
	}
	private static void ch2_1main(){
		Account ac=new Account();
		Account ca=new ChekingAccount();
		Account cla=new CreditLineAccount();

		Account ctca=new ChekingTrafficCardAccount();
	}
}*/

