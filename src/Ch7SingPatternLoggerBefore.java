public class Ch7SingPatternLoggerBefore{
	private static Ch7SingPatternLoggerBefore uniqueInstance;
	public Ch7SingPatternLoggerBefore(){}
	public static synchronized Ch7SingPatternLoggerBefore getInstance(){
		if(uniqueInstance==null)
			uniqueInstance=new Ch7SingPatternLoggerBefore();
		return uniqueInstance;
}
	public void readEntireLog(){
		System.out.println("Singleton log goes here");
	}
}
