interface Comparator
{
	public int compare(Object o1, Object o2);
	public boolean equals(Object o);
}
class StringComparator implements Comparator
{
	public int compare(Object o1, Object o2)
	{
		String s1=(String) o1;
		String s2=(String) o2;
		return s1.compareTo(s2);
	}
}
class IntegerComparator implements Comparator
{
	public int compare(Object o1, Object o2)
	{
		return (Integer) o1 - (Integer) o2;
	}
}
class Sorter
{
	public static void sort(Object[] data, Comparator comp)
	{
		for (int i = data.length-1; i>=1; i--){
			int indexOfMax=0;
			for (int j=1;j<=i;j++){
				if (comp.compare(data[j], data[indexOfMax])>0)
					indexOfMax=j;
			}
		Object temp=data[i];
		data[i]=data[indexOfMax];
		data[indexOfMax]=temp;
		}
	}
}
class Sorter_v4
{
	public static void main(String[] args)
	{
		String[] B = {"John", "Adams", "Skrien", "Smith", "Jones"};
		Comparator stringComp=new StringComparator();
		Sorter.sort(B, stringComp);
		Integer[] C = {new Integer(3), new Integer(1), new Integer(4), new Integer(2)};
		Comparator integerComp=new IntegerComparator();
		Sorter.sort(C, integerComp);

System.out.println("before=====================");
        for (int i = 0; i < C.length; i++) {
            System.out.println("numbers["+i+"]="+C[i]);
        }
        for (int i = 0; i < B.length; i++) {
            System.out.println("strings["+i+"]="+B[i]);
        }
                        //--------------------------------------------------------------------------------------------------                                     
Sorter.sort(C, integerComp);
        Sorter.sort(B, integerComp);
                        //--------------------------------------------------------------------------------------------------
        System.out.println("after======================");

        for (int i = 0; i < C.length; i++) {
            System.out.println("numbers["+i+"]="+C[i]);
        }
        for (int i = 0; i < B.length; i++) {
            System.out.println("strings["+i+"]="+B[i]);
        }
    }
}

